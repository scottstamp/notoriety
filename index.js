require("dotenv").config();

const http = require("http");
const PORT = process.env.PORT || 5050;

const Web3 = require('web3');
var web3 = new Web3(null);

const account = web3.eth.accounts.privateKeyToAccount(process.env.KEY);

function getReqData(req) {
    return new Promise((resolve, reject) => {
        try {
            let body = "";
            req.on("data", (chunk) => {
                body += chunk.toString();
            });
            req.on("end", () => {
                resolve(body);
            });
        } catch (error) {
            reject(error);
        }
    });
}

const server = http.createServer(async (req, res) => {
    if (req.url === "/sign" && req.method === "POST") {
        var body = JSON.parse(await getReqData(req));
        if (body.nonce !== undefined) {
            try {
                var nonceBuf = Buffer.from(body.nonce.trim(), 'base64');
                var nonce = nonceBuf.toString('base64');

                // check if nonce is a 32 byte base64 string, with no other words surrounding it
                if (nonceBuf.length === 32 && nonce === body.nonce) {
                    res.writeHead(200, { "Content-Type": "application/json" });
                    // return signed nonce
                    res.end(JSON.stringify({ signature: account.sign(nonce).signature }));
                } else {
                    // return error if the signature request is anything other than a plain base64 string
                    res.writeHead(500, { "Content-Type": "application/json" });
                    res.end(JSON.stringify({ message: "signature request origin could not be determined" }));
                }
                return;
            } catch (error) {
                res.writeHead(500, { "Content-Type": "application/json" });
                res.end(JSON.stringify({ message: error }));
                return;
            }
        }
    } else {
        res.writeHead(404, { "Content-Type": "application/json" });
        res.end(JSON.stringify({ message: "invalid request" }));
    }
})

server.listen(PORT, () => {
    console.log(`server started on port: ${PORT}`);
});
